FROM php:7.4-apache
RUN apt-get update -y
RUN apt-get install -y git
WORKDIR /var/www
RUN git clone https://gitlab.com/jovannserreau/tp-pass.git api
RUN a2enmod rewrite
RUN docker-php-ext-install mysqli
RUN docker-php-ext-install pdo_mysql
RUN apt-get install -y libzip-dev
RUN pecl install xdebug
RUN docker-php-ext-enable xdebug
RUN docker-php-ext-install zip
ADD vhostApi.conf /etc/apache2/sites-available/api.conf
RUN a2dissite 000-default.conf
RUN a2ensite api.conf
RUN curl -sS https://getcomposer.org/installer -o composer-setup.php
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer
WORKDIR /var/www/api
RUN chown -R www-data:www-data .
RUN composer install
EXPOSE 80