<?php


namespace App\Tests;


use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ProductTest extends KernelTestCase
{
        public function testNombreProduct(){
            self::bootKernel();
            $users = self::$container->get(ProductRepository::class)->count([]);
            $this->assertEquals(21,$users);
        }
}